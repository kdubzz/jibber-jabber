---
title: "Creating a static website with Hugo - What I learned (part 2)"
date: 2020-06-14T08:42:00-07:00
draft: true
---

### TL;DR Summary
- Check out these links to help wrap your mind around the Hugo file structure,
  - [Mike Dane's Giraffe Academy](https://www.youtube.com/watch?v=qtIqKaDlqXo&list=PLLAZ4kZ9dFpOnyRlyS-liKL5ReHDcj4G3)
  - [Jake Wiesler's Blog](https://www.jakewiesler.com/blog/hugo-directory-structure/)
  - [Hugo Docs](https://gohugo.io/getting-started/directory-structure/), which is pretty good!

Continuing from _Part 1_, I was talking of the file structure of a Hugo project.

> ```
 ./<root_dir>
    archetypes/
    content/
    data/
    layouts/
    resources/
    static/
    themes/
    config.toml
```

Given the extensive nature of Hugo's documentation, I'm going to discuss the things that 
had me pretty puzzled when I started this mini-project. 

---

### _Archetypes_ Folder

What are _"Archetypes"_?.  By definition, an _archetype_ (noun) is an [_"original pattern or model from which all things of the same kind are copied..."_](https://www.dictionary.com/browse/archetype).
When a new file is generated via. `Hugo`, the new file generated (_e.g._ "example.md") has
a default _front-matter_ (specified in `./archtypes/default.md` like the following

> ```
---
title: <file-title>
date: YYYY-MM-DDTHH:MM:ss
draft: true
---
```

The `./archetypes/default.md` file simply specifies what type of _front-matter_ info is created
when a new _content_ file is created using the `hugo new` command.
For example, if I changed `./archetypes/default.md` file to include an author,

> ```
---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
draft: true
author: "Kang Wang"
---
```

Every subsequent file created using `hugo new <file_name.md>` should have the new front-matter specified in
`./archetypes/default.md`. From the [Hugo Docs](https://gohugo.io/getting-started/directory-structure/),
the _archetypes_ stores content files that specify _custom front-matter_ fdor newly created hugo files.
I followed along with [Mike Dane's Giraffe Academy tutorial lessons](https://www.youtube.com/watch?v=bcme8AzVh6o&list=PLLAZ4kZ9dFpOnyRlyS-liKL5ReHDcj4G3&index=8)
as a compliment to the documentary to the get up to speed fairly quickly.

Given the definition of an _archetype_ above, the function of an archetype file becomes much clearer, as all newly created files will _inherit_ the 
_front-matter_ details specified within the archetype content file.

---

### _Contents_ Folder

Pretty self-explanatory - the contents of your static website.

One thing that may _not_ be obvious is that any top-level folders here will specify the different sections of your website.
So, files stored in `./contents/pages/<file_name.md>` will be rendered at the url: `<your_website_name>/pages/<file_name>`.

These top-level folders do not need to be created manually. They can be generated _via._ the `hugo` cli.
By default, running `hugo new <file_name.md>` will generate `<file_name.md>` in the contents folder (_e.g._ `./contents/<file_name.md>`),
but if the command `hugo new pages/<file_name.md>` was run instead, then `<file_name.md>` wouldd be placed in `./contents/pages/<file_name.md>`, and 
the folder _pages_ is automatically created within the _contents_ folder.

---

### _Layout_ Folder

This is where all the static `HTML` files are stored.

In Hugo, there are _primarily_ **two** types of template files - **_List_** template files, and  **_Single_** template files.
Template files include the following,

  - _List_ pages 
    - Templates used to render _multiple_ pieces of content (_e.g._ multiple posts on a blog page)
  - _Home_ page
    - Unique pages with different formatting, and also has the `Site` and `Page` variables available for use.
  - _Taxonomy_ templates
  - _Partials_
  - _Single Page Templates_
