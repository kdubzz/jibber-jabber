# README

Building my first static website with Hugo, and documenting all the learnings in the process

So far, so good, and I've been able to get a first post up.
Next, is to roll this out to GitLab Pages.

![home_page]("https://gitlab.com/kdubzz/jibber-jabber/-/blob/master/repo_images/jibber_jabber1.png", "Home Page")

![first_post]("https://gitlab.com/kdubzz/jibber-jabber/-/blob/master/repo_images/jibber_jabber2.png", "First Post")
